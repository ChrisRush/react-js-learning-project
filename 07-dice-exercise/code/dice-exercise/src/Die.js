import React, {Component} from "react";
import './Die.css'

const spelledValues = {
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
    5: 'five',
    6: 'six'
}

class Die extends Component {
    render() {
        return (
            <div className={"Die"}>
                <i className={`Die-icon fas fa-dice-${spelledValues[this.props.face]} ${this.props.rolling && 'shaking'}`} />
            </div>
        )
    }
}

export default Die;