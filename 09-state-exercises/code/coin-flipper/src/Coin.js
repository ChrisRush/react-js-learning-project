import React, {Component} from "react";
import './Coin.css'
import heads from './heads.png'
import tails from './tails.png'

class Coin extends Component {
    static defaultProps = {
        face: 'heads'
    }
    render() {
        return (
            <div className={'Coin'}>
                {this.props.face === 'heads' && <img src={heads} alt={'heads'}/>}
                {this.props.face === 'tails' && <img src={tails} alt={'tails'}/>}
            </div>
        )
    }
}

export default Coin;