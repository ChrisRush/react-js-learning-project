import React, {Component} from "react";
import './RollDice.css'
import Die from "./Die";

class RollDice extends Component {
    state = {
        die1: 1,
        die2: 1,
        roll: false
    }

    roll = () => {
        let newDie1 = Math.floor(Math.random() * 6) + 1
        let newDie2 = Math.floor(Math.random() * 6) + 1
        this.setState({
            die1: newDie1,
            die2: newDie2,
            roll: true
        })
        setTimeout(() => {
            this.setState({roll: false})
        }, 1000)
    }

    render() {
        return (
            <div className={"RollDice"}>
                <div className={'RollDice-container'}>
                    <Die face={this.state.die1} rolling={this.state.roll}/>
                    <Die face={this.state.die2} rolling={this.state.roll}/>
                </div>
                <button onClick={this.roll} disabled={this.state.roll}>
                    {this.state.roll ? `Rolling...` : `Roll Dice`}
                </button>
            </div>
        )
    }
}

export default RollDice;