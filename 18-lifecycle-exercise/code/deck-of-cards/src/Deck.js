import React, {Component} from "react";
import axios from "axios";
import Card from "./Card";
import './Deck.css'

const API_BASE_URL = 'https://deckofcardsapi.com/api/deck';

class Deck extends Component {

    constructor(props) {
        super(props);
        this.state = {
            deck: null,
            drawn: []
        }
    }

    async componentDidMount() {
        let deck = await axios.get(`${API_BASE_URL}/new/shuffle`);
        this.setState({
            deck: deck.data
        })
        console.log(this.state.deck)
    }

    getCard = async () => {
        let id = this.state.deck.deck_id

        try {
            let cardUrl = `${API_BASE_URL}/${id}/draw/`
            let cardRes = await axios.get(cardUrl)
            if (!cardRes.data.success) {
                throw new Error('No card remaining')
            }

            console.log(cardRes.data)
            let card = cardRes.data.cards[0]
            this.setState(st => ({
                drawn: [
                    ...st.drawn,
                    {
                        id: card.code,
                        image: card.image,
                        name: `${card.suit} ${card.value}`
                    }
                ]
            }))
        } catch (err) {
            alert(err)
        }
    }

    render() {
        const cards = this.state.drawn.map(c => (
            <Card name={c.name} image={c.image} key={c.id} />
        ))
        return (
            <div className={'Deck'}>
                <h1 className={'Deck-title'}>&#9826; Card Dealer &#9826;</h1>
                <h2 className={'Deck-title subtitle'}>&#9826; A little demo made with React &#9826;</h2>
                <button className={'Deck-btn'} onClick={this.getCard}>Get Card</button>
                <div className={'Deck-cardarea'}>
                    {cards}
                </div>
            </div>
        )
    }
}

export default Deck