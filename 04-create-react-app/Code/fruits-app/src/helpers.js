const choice = function ( arr: [] ) {
    return arr[Math.floor(Math.random() * arr.length)]
}

const remove = function ( arr: [], item ) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === item) {
            arr.splice(i, 1);
        }
    }
}

export { choice, remove }