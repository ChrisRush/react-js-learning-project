import React, {Component} from "react";
import './Pokecard.css'

const  POKE_API = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/`

class Pokecard extends Component {
    render() {
        let imgSrc = createProperImgSrc(this.props.id)
        return (
            <div className={'Pokecard'}>
                <h2 className={`Pokecard-title`}>{this.props.name}</h2>
                <div className={`Pokecard-image`}>
                    <img src={imgSrc} alt={`${this.props.name} image`}/>
                </div>
                <div className={`Pokecard-data`}>Type: {this.props.type}</div>
                <div className={`Pokecard-data`}>EXP: {this.props.exp}</div>
            </div>
        )
    }
}

function createProperImgSrc(id: Number): String {
    let properId;
    if (id > 0 && id < 10) {
        properId = '00' + id;
    } else if (id > 9 && id < 100) {
        properId = '0' + id;
    } else
        properId = id;

    return `${POKE_API}${properId}.png`
}

export default Pokecard;