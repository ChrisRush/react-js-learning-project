import React, {Component} from "react";

class Clicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 0
        }
    }

    handleClick = (e) => {
        let rand = Math.floor(Math.random() * 10) + 1
        this.setState({number: rand})
    }

    render() {
        return (
            <div className={"Clicker"}>
                <h2>Number is {this.state.number}</h2>
                {this.state.number === 7 ? (
                    <h2>Winner</h2>
                ) : (
                    <button onClick={this.handleClick}>Random Number</button>
                )}
            </div>
        )
    }
}

export default Clicker;