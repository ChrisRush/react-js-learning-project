import React, {Component} from "react";
import Coin from "./Coin";

class CoinFlipper extends Component {
    state = {
        face: 'heads',
        total: 0,
        heads: 0,
        tails: 0
    }

    flipCoin = () => {
        let faceTmp = Math.floor(Math.random() * 2) === 0 ? 'heads' : 'tails'
        this.setState(currState => ({
            face: faceTmp,
            total: currState.total + 1,
            heads: currState.heads + (faceTmp === 'heads' ? 1 : 0),
            tails: currState.tails + (faceTmp === 'tails' ? 1 : 0)
        }))
    }

    render() {
        return (
            <div className={'CoinFlipper'}>
                <h2>Let's flip a coin!</h2>
                <Coin face={this.state.face}/>
                <p>Total flips: {this.state.total} Heads: {this.state.heads} Tails: {this.state.tails}</p>
                <button onClick={this.flipCoin}>Flip a Coin</button>
            </div>
        )
    }
}

export default CoinFlipper;