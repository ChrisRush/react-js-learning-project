import React, {Component} from "react";
import {v4} from 'uuid';
import NewBoxForm from "./NewBoxForm";
import Box from "./Box";

class BoxList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            boxes: []
        }
    }

    addBox = (box) => {
        let newBox = {...box, id: v4()}
        this.setState(state => ({
            boxes: [...state.boxes, newBox]
        }))
    }

    render() {
        const boxes = this.state.boxes.map(box => (
            <Box
                key={box.key}
                id={box.id}
                height={box.height}
                width={box.width}
                color={box.color}
            />
        ))
        return (
            <div>
                <NewBoxForm addBox={this.addBox}/>
                {boxes}
            </div>
        )
    }
}

export default BoxList;